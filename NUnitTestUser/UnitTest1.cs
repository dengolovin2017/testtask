using NUnit.Framework;
using ConsoleApp3;

namespace NUnitTestUser
{
    public class Tests
    {
        private User user;

        [SetUp]
        public void Setup()
        {
        }

        /**
         Тестирование генерации токена
        */
        [Test]
        public void TestGenerateToken()
        {
            user = new User("login", "password");
            user.GenerateNewToken();
            var result = user.AuthToken;
            Assert.AreEqual(!string.IsNullOrEmpty(result), true);
        }


        /**
         Тестирование генерации исключения при генерации токена
        */
        [Test]
        public void TestGenerateExceptionAuthenticateUser()
        {
            user = new User(null, "password");
            Assert.Throws<System.ArgumentNullException>(() => user.GenerateNewToken());
        }

        /**
         Тестирование получения токена
        */
        [Test]
        public void TestGenerateSecondToken()
        {
            user = new User("login", "password");
            user.GenerateNewToken();
            var result1 = user.AuthToken;
            System.Threading.Thread.Sleep(1000);
            user.GenerateNewToken();
            var result2 = user.AuthToken;
            Assert.AreNotEqual(result1, result2);
        }
    }
}
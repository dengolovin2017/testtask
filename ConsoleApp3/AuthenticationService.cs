﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp3
{
    class AuthenticationService : IAuthenticationService
    {
        public AuthToken Authenticate(string username, string password)
        {

            if (String.IsNullOrEmpty(username) || String.IsNullOrEmpty(password))
            {
                throw new ArgumentNullException("Error");
            }
            AuthToken token = new AuthToken();
            DateTime date = DateTime.Now.AddYears(1);
            token.Token = username + password + date;
            token.ExpiresAt = date;

            return token;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp3
{
    interface IUser
    {
        /// <summary>
        /// User's authentication token.
        /// </summary>
        String AuthToken { get; }

    }
}

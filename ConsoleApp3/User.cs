﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp3
{
    public class User : IUser
    {
        private String username;
        private String password;
        private IAuthenticationService authenticationService;
        private AuthToken token;

        public User(String username, String password)
        {
            this.username = username;
            this.password = password;
            authenticationService = new AuthenticationService();
        }

        public void GenerateNewToken()
        {
            token = authenticationService.Authenticate(username, password); 
        }

        public string AuthToken
        {
            get
            {
                return token.Token;
            }
        }
    }
}

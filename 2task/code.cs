[WebMethod]
public Order LoadOrderInfo(string orderCode)
{
    try
    {
        Debug.Assert(!String.IsNullOrEmpty(orderCode));
        Stopwatch stopWatch = new Stopwatch();
        stopWatch.Start();

        lock (cache)
        {
            if (cache.ContainsKey(orderCode))
            {
                stopWatch.Stop();
                logger.Log("INFO", "The order was taken from the cache. Elapsed - {0}", stopWatch.Elapsed);

                return cache[orderCode];
            }
        }

        string queryTemplate = "SELECT OrderID, CustomerID, TotalMoney FROM dbo.Orders where OrderCode='{0}'";
        string query = string.Format(queryTemplate, orderCode);
        SqlConnection connection = new SqlConnection(this.connectionString);
        SqlCommand command = new SqlCommand(query, connection);
        connection.Open();
        SqlDataReader reader = command.ExecuteReader();
        int orderNumberID = 0, customerNumberID = 1 TotalMoneyNumber = 2;

        if (reader.HasRows)
        {
            reader.Read();
            Order order = new Order((string)reader[orderNumberID], (string)reader[customerNumberID], (int)reader[TotalMoneyNumber]);

            lock (cache)
            {
                if (!cache.ContainsKey(orderCode))
                    cache[orderCode] = order;
            }
            stopWatch.Stop();
            logger.Log("INFO", "The order was taken from the database. Elapsed - {0}", stopWatch.Elapsed);

            return order;
        }

        stopWatch.Stop();
        logger.Log("INFO", "The order was not found. Elapsed - {0}", stopWatch.Elapsed);

        return null;
    }
    catch (SqlException ex)
    {
        logger.Log("ERROR", ex.Message);
        throw new ApplicationException("Error");
    }
    finally
    {
        reader.Close();
        connection.Close();
    }
}

public IDictionary<string, Order> cache;

